(ns clj-refile.providers.extras.azure-credentials
  (:require [clj-refile.parse :as p]
            [clojure.string :as string]
            [clojure.set :as cs])
  (:import (com.azure.identity DefaultAzureCredentialBuilder ManagedIdentityCredentialBuilder)))

(defn authenticate [client-builder {:keys [sas-token managed-identity managed-identity-builder]}]
  (cond
    sas-token
    (.sasToken client-builder sas-token)
    managed-identity-builder
    (.credential client-builder
                 (-> (ManagedIdentityCredentialBuilder.)
                     (#(if managed-identity (.clientId % managed-identity) %))
                     (.build)))
    :else
    (.credential client-builder
                 (-> (DefaultAzureCredentialBuilder.)
                     (#(if managed-identity (.managedIdentityClientId % managed-identity) %))
                     (.build)))))

(defn- remove-nils [m]
  (into {} (remove (comp nil? second) m)))

(defn- az-rename-keys [m]
  (cs/rename-keys m {:container-name :file-container
                     :file-system    :file-container
                     :share-name     :file-container
                     :blob-name      :file-location
                     :blob-path      :file-location
                     :resource-path  :file-location}))

(defn- query-sas-token? [uri-query]
  (some #(string/starts-with? (or uri-query "") %) ["sv=" "st="]))

(defn- uri-map->azure [uri-map]
  (let [path-parts (filter (comp not string/blank?) (string/split (:path uri-map) #"\/"))]
    (remove-nils
     {:endpoint       (str (:scheme uri-map) "://" (:host uri-map))
      :sas-token      (when (query-sas-token? (:query uri-map))
                        (str "?" (:query uri-map)))
      :file-container (first path-parts)
      :file-location  (string/join "/" (rest path-parts))})))

(defn uri->azure-parsed
  [uri-string conn-props]
  (let [uri-map (p/parse-uri uri-string)]
    (merge
     (az-rename-keys conn-props)
     (if (some #{(string/lower-case (or (:scheme uri-map) ""))} ["http" "https"])
       (uri-map->azure uri-map)
       {:file-location uri-string}))))
