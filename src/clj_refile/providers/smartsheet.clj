(ns clj-refile.providers.smartsheet
  (:require [clj-refile.parse :as p]
            [clj-refile.protocols :refer [FileProtocol]]
            [clojure.java.io :as io]
            [clojure.string :as string]
            [taoensso.timbre :as log])
  (:import (com.smartsheet.api Smartsheet SmartsheetBuilder SheetResources)
           (com.smartsheet.api.models.enums PaperSize)))

(defn- create-sheet-resources
  [{:keys [access-token]}]
  (log/debug "Creating Smartsheet Resource Client")
  (let [^Smartsheet sh (-> (SmartsheetBuilder.)
                           (.setAccessToken access-token)
                           (.build))]
    (.sheetResources sh)))

(defn- uri->sheet-id
  [uri-string]
  (let [uri-map (p/parse-uri uri-string)]
    (as->
     (:path uri-map) p
      (subs p (+ (string/index-of p "/sheets/") 7))
      (string/split p #"\/")
      (get p 1)
      (biginteger p))))

(defn- get-sheet-as [^SheetResources sr sheet-id output]
  (with-open [writer  (io/output-stream (:filepath output))]
    (cond
      (string/ends-with? (:filepath output) ".pdf") (.getSheetAsPDF sr sheet-id writer PaperSize/A1)
      (string/ends-with? (:filepath output) ".xlsx") (.getSheetAsExcel sr sheet-id writer)
      :else (.getSheetAsCSV sr sheet-id writer))))

(defrecord SmartsheetFile [scheme filepath conn-props]
  FileProtocol
  (delete-file [_]
    (let [^SheetResources sr (create-sheet-resources conn-props)
          sheet-id (uri->sheet-id filepath)]
      (.deleteSheet sr sheet-id))
    true)
  (delete-if-exists [this]
    (when (.exists? this) (.delete-file this)))
  (download [_ output]
    (let [^SheetResources sr (create-sheet-resources conn-props)
          sheet-id (uri->sheet-id filepath)]
      (.make-parents output)
      (.delete-if-exists output)
      (get-sheet-as sr sheet-id output))
    (:filepath output))
  (exists? [_]
    (let [^SheetResources sr (create-sheet-resources conn-props)
          sheet-id (uri->sheet-id filepath)]
      (try
        (.getSheet sr sheet-id nil nil nil nil nil nil nil)
        true
        (catch Exception _
          false))))
  (list-files [this]
    (log/warn "Listing files not supported for Smartsheets. Returning filepath if sheet exists.")
    (when (.exists? this)
      [filepath])))