(ns clj-refile.fixtures
  (:require [clojure.spec.test.alpha :as stest]
            [clojure.java.io :as io]))

(defn instrument [f]
  (stest/instrument f))

(defn qc-test [f]
  (stest/instrument f)
  (let [res (:failure (stest/abbrev-result (first (stest/check f))))]
    (when-not (nil? res)
      (println res))
    (nil? res)))

;(defn stest-failure-simple [f]
;  (stest/instrument f)
;  (is (nil? (stest/check f))))

(defn- setup-dirs
  [dirs]
  (doseq [d dirs]
    (io/make-parents (str d "/dummy.txt"))))

(defn create-files
  ([files content]
   (doseq [f files]
     (spit f content)))
  ([files]
   (create-files files "test")))

(defn- delete-recursively
  "Used for cleanup..."
  [filename]
  (let [func (fn [func f]
               (when (.isDirectory f)
                 (doseq [f2 (.listFiles f)]
                   (func func f2)))
               (io/delete-file f true))]
    (func func (io/file filename))))

(defn- cleanup-dirs
  [dirs]
  (doseq [d dirs]
    (delete-recursively d)))

(defn st-fixture
  [f & [{:keys [pre-fcs post-fcs create-dirs]}]]
  (setup-dirs create-dirs)
  (doseq [fc pre-fcs]
    (fc))
  (f)
  (doseq [fc post-fcs]
    (fc))
  (cleanup-dirs create-dirs))