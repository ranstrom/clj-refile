(ns clj-refile.providers.sftp
  (:require [clj-ssh.ssh :as ssh]
            [clj-refile.parse :as p]
            [clj-refile.protocols :refer [FileProtocol]]
            [taoensso.timbre :as log]))

(defn- ls-entry>name
  [input]
  (vec
   (map #(.getFilename %) input)))

(defn- get-connection-info
  [{:keys [user pswd port]}]
  (merge {:strict-host-key-checking :no}
         (when user {:username user})
         (when pswd {:password pswd})
         (when port {:port port})))

(defn- sftp-connect
  [{:keys [host ssh-key] :as conn-props} input-func]
  (let [agent (ssh/ssh-agent {:use-system-ssh-agent false})]
    (when ssh-key
      (ssh/add-identity agent {:private-key-path ssh-key}))
    (let [session (ssh/session agent host (get-connection-info conn-props))]
      (ssh/with-connection
        session
        (let [channel (ssh/ssh-sftp session)]
          (ssh/with-channel-connection channel
            (input-func channel)))))))

(defrecord SFTPFile [scheme filepath conn-props]
  FileProtocol
  (delete-file [_]
    (sftp-connect
     conn-props
     (partial
      (fn [fp channel]
        (ssh/sftp channel {} :put fp (:path (p/parse-uri fp))))
      filepath))
    true)
  (delete-if-exists [this]
    (when (.exists? this) (.delete-file this)))
  (download [_ output]
    (.make-parents output)
    (sftp-connect
     conn-props
     (partial
      (fn [rp lp channel]
        (ssh/sftp channel {} :get (:path (p/parse-uri rp)) lp)
        lp)
      filepath
      (:filepath output)))
    (:filepath output))
  (exists? [_]
    (sftp-connect
     conn-props
     (partial
      (fn [fp channel]
        (try
          (ssh/sftp channel {} :ls (:path (p/parse-uri fp)))
          true
          (catch Exception e
            (log/debug (str "File doesn't exist: " e))
            false)))
      filepath)))
  (list-files [_]
    (sftp-connect
     conn-props
     (partial
      (fn [fp channel]
        (let [uri-path (:path (p/parse-uri fp))
              file-pattern (p/get-file-pattern
                            (p/get-filename-from-path uri-path))
              path (p/get-dir-from-path uri-path)]
          (->> (ssh/sftp channel {} :ls path)
               (ls-entry>name)
               (p/get-file-matches file-pattern)
               (p/append-dir-to-files (p/get-dir-from-path fp)))))
      filepath)))
  (upload [_ input]
    (sftp-connect
     conn-props
     (partial
      (fn [lp rp channel]
        (ssh/sftp channel {} :put lp (:path (p/parse-uri rp)))
        rp)
      (:filepath input)
      filepath))
    filepath))