# clj-refile

Generic library for operating on and interacting with files and providers/protocols.

The basis is getting a `clj-refile.protocols.File` defrecord (recommend using `clj-refile.core/->file`). Depending on file-path(s) provided, common file-operations will be available like:

- checksum
- copy
- download
- exists?
- list-files
- upload

Additionally, `clj-refile.parse` provides common ways of parsing and otherwise obtaining file-related information.

## Protocols/Providers

- azure-blob
- azure-data-lake
- ftp
- http/https
- sftp

## Resources

- [Azure Blob vs. Data Lake](https://docs.microsoft.com/en-us/azure/data-lake-store/data-lake-store-comparison-with-blob-storage)

## Disclaimer

**THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.**
