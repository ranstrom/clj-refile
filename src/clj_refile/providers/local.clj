(ns clj-refile.providers.local
  (:require [clj-refile.parse :as p]
            [clj-refile.protocols :refer [FileProtocol]]
            [clojure.java.io :as io]
            [digest :as digest]))

(defrecord File [scheme filepath conn-props]
  FileProtocol
  (checksum [_]
    (-> filepath io/as-file digest/sha-256))
  (copy [_ output]
    (.make-parents output)
    (let [out (:filepath output)]
      (when (not= filepath out)
        (io/make-parents out)
        (io/copy (io/file filepath) (io/file out)))
      out)
    (:filepath output))
  (delete-file [_]
    (io/delete-file filepath)
    true)
  (delete-if-exists [this]
    (when (.exists? this) (.delete-file this)))
  (exists? [_]
    (.exists (io/as-file filepath)))
  (list-files [_]
    (let [files-in-dir (-> filepath p/get-dir-from-path io/file (.listFiles))]
      (->> (p/get-file-pattern filepath)
           (#(p/get-file-matches % files-in-dir))
           (map p/update-win-style-paths))))
  (make-parents [_]
    (try
      (io/make-parents filepath)
      filepath
      (catch Exception e
        (throw (Exception. (str "Failed creating directories for path: " filepath ", " e)))))))