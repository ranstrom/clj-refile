(ns clj-refile.core-test
  (:require [clojure.test :refer [deftest is testing use-fixtures]]
            [clj-refile.core :as fh :refer [->file]]
            [clj-refile.fixtures :as fx]
            [clj-refile.protocols :as p]
            [clj-refile.providers.ftp :as fh-ftp]
            [clj-refile.providers.sftp :as fh-sftp]
            [clj-ssh.ssh :as ssh]
            [miner.ftp :as ftp]))

(def file-dir-atom (atom (str "./" (fh/uuid))))

(defn- st-file-fixture [f]
  (fx/st-fixture f {:create-dirs [@file-dir-atom]}))

(use-fixtures :once st-file-fixture)

;; (deftest test-providers
;;   (testing "Loading all providers"
;;     (let [f #(-> % :fn-map :exists?)]
;;       (is (f (->file "/some/path" {:scheme :azure-blob})))
;;       (is (f (->file "/some/path" {:scheme :azure-data-lake})))
;;       (is (f (->file "/some/path" {:scheme :ftp})))
;;       (is (f (->file "/some/path" {:scheme :http})))
;;       (is (f (->file "/some/path" {:scheme :local})))
;;       ; (is (f (->file "/some/path" {:scheme :oci})))
;;       (is (f (->file "/some/path" {:scheme :sftp}))))))

(deftest test-get-file-checksum
  (testing "Getting file checksum"
    (is (.checksum (->file "./deps.edn")))))

(deftest test-ftp
  (with-redefs [fh-ftp/ftp-connect (fn [_ _ func] (func {}))
                p/make-parents (fn [_] true)]
    (with-redefs [ftp/client-file-names (constantly
                                         (vector "not-file.txt"
                                                 "file.txt"))]
      (is (.exists? (->file "/some/path/file.txt" {:scheme :ftp}))))
    (with-redefs [ftp/client-file-names (constantly
                                         (vector "not-file.txt"))]
      (is (not (.exists? (->file "/some/path/file.txt" {:scheme :ftp})))))

    (with-redefs [ftp/client-file-names (constantly
                                         (vector "file1.csv"
                                                 "file2.csv"
                                                 "file3.txt"
                                                 "nfile4.csv"))]
      (is (= 2 (count (.list-files (->file "ftp://host:port/some/path/file*.csv"))))))
    (with-redefs [ftp/client-get (constantly true)]
      (is (= "/local/path"
             (.download (->file "/remote/path" {:scheme :ftp})
                        (->file "/local/path")))))

    (with-redefs [ftp/client-put (constantly true)]
      (is (= "/remote/path"
             (.upload (->file "/remote/path" {:scheme :ftp})
                      (->file "/local/path")))))

    ;(with-redefs [ftp/client-delete (constantly true)]
    ;  (is (fh-ftp/delete-file "/some/path" {})))
    ))

(deftest test-local
  (testing "non-existent properties files are bad"
    (let [file-exists (.exists? (->file "/some/random/path.json"))]
      (is (false? file-exists))))
  (testing "listing files"
    (fx/create-files [(str @file-dir-atom "/file1.txt")
                      (str @file-dir-atom "/file2.txt")])
    (is (= 2 (count (.list-files (->file (str @file-dir-atom "/file*.txt"))))))))

(deftest test-sftp
  (with-redefs [fh-sftp/sftp-connect (fn [_ func] (func {}))]
    (with-redefs [ssh/sftp (constantly true)]
      (is (.exists? (->file "/some/path" {:scheme :sftp}))))
    (with-redefs [ssh/sftp (fn []
                             (throw (Exception. (str "Not found"))))]
      (is (not (.exists? (->file "/some/path" {:scheme :sftp})))))

    (with-redefs [ssh/sftp (constantly
                            (vector "file1.csv"
                                    "file2.csv"
                                    "file3.txt"
                                    "nfile4.csv"))
                  fh-sftp/ls-entry>name (fn [x] x)]
      (is (= 2 (count (.list-files (->file "sftp://host:port/some/path/file*.csv"))))))

    (with-redefs [ssh/sftp (constantly true)]
      (is (= "/local/path"
             (.download (->file "/remote/path" {:scheme :sftp})
                        (->file "/local/path")))))

    (with-redefs [ssh/sftp (constantly true)]
      (is (= "/remote/path"
             (.upload (->file "/remote/path" {:scheme :sftp})
                      (->file "/local/path")))))

    (with-redefs [ssh/sftp (constantly true)]
      (is (.delete-file (->file "/some/path" {:scheme :sftp}))))))

; (deftest test-shared-file-functions
;  (testing "exists?"
;    (with-redefs [fh-ftp/exists? (constantly "ftp-ret")
;                  fh-http/exists? (constantly "http-ret")
;                  fh-sftp/exists? (constantly "sftp-ret")
;                  fh-oci/exists? (constantly "oci-ret")]
;      (is (= (.exists? (->file "ftp://host:80/some/file/path.txt")) "ftp-ret"))
;      (is (= (.exists? (->file "https://host:80/some/file/path.txt")) "http-ret"))
;      (is (= (.exists? (->file "http://host:80/some/file/path.txt")) "http-ret"))
;      (is (= (.exists? (->file "sftp://host:80/some/file/path.txt")) "sftp-ret"))
;      (is (= (.exists? (->file "oci://host:80/some/file/path.txt")) "oci-ret"))
;      (is (not (.exists? (->file "/some/random/path.txt"))))
;      (is (thrown? Exception (.exists? (->file "s3://unsupported/today"))))))
;  (testing "list-files"
;    (with-redefs [fh-ftp/list-files (constantly "ftp-ret")
;                  fh-http/list-files (constantly "http-ret")
;                  fh-sftp/list-files (constantly "sftp-ret")
;                  fh-oci/list-files (constantly "oci-ret")
;                  fh-local/list-files (constantly "local-ret")]
;      (is (= (.list-files (->file "ftp://host:80/some/file/path.txt")) "ftp-ret"))
;      (is (= (.list-files (->file "https://host:80/some/file/path.txt")) "http-ret"))
;      (is (= (.list-files (->file "http://host:80/some/file/path.txt")) "http-ret"))
;      (is (= (.list-files (->file "sftp://host:80/some/file/path.txt")) "sftp-ret"))
;      (is (= (.list-files (->file "oci://host:80/some/file/path.txt")) "oci-ret"))
;      (is (= (.list-files (->file "/some/random/path.txt")) "local-ret"))
;      (is (thrown? Exception (.list-files (->file "s3://unsupported/today"))))))
;  (testing "download"
;    (with-redefs [fh-ftp/download  (constantly "ftp-ret")
;                  fh-http/download  (constantly "http-ret")
;                  fh-sftp/download  (constantly "sftp-ret")
;                  fh-oci/download  (constantly "oci-ret")
;                  fh-local/download  (constantly "local-ret")]
;      (is (= (.download (->file "ftp://host:80/some/file/path.txt")) "ftp-ret"))
;      (is (= (.download (->file "https://host:80/some/file/path.txt")) "http-ret"))
;      (is (= (.download (->file "http://host:80/some/file/path.txt")) "http-ret"))
;      (is (= (.download (->file "sftp://host:80/some/file/path.txt")) "sftp-ret"))
;      (is (= (.download (->file "oci://host:80/some/file/path.txt")) "oci-ret"))
;      (is (= (.download (->file "/some/random/path.txt")) "local-ret"))
;      (is (thrown? Exception (.download (->file "s3://unsupported/today"))))))
;  (testing "delete-file"
;    (with-redefs [fh-ftp/delete-file (constantly "ftp-ret")
;                  fh-http/delete-file (constantly "http-ret")
;                  fh-sftp/delete-file (constantly "sftp-ret")
;                  fh-oci/delete-file (constantly "oci-ret")
;                  fh-local/delete-file (constantly "local-ret")]
;      (is (= (.delete-file (->file "ftp://host:80/some/file/path.txt")) "ftp-ret"))
;      (is (= (.delete-file (->file "https://host:80/some/file/path.txt")) "http-ret"))
;      (is (= (.delete-file (->file "http://host:80/some/file/path.txt")) "http-ret"))
;      (is (= (.delete-file (->file "sftp://host:80/some/file/path.txt")) "sftp-ret"))
;      (is (= (.delete-file (->file "oci://host:80/some/file/path.txt")) "oci-ret"))
;      (is (= (.delete-file (->file "/some/random/path.txt")) "local-ret"))
;      (is (thrown? Exception (.delete-file (->file "s3://unsupported/today"))))))
;  (testing "upload"
;    (with-redefs [fh-ftp/upload  (constantly "ftp-ret")
;                  fh-http/upload  (constantly "http-ret")
;                  fh-sftp/upload  (constantly "sftp-ret")
;                  fh-oci/upload  (constantly "oci-ret")
;                  fh-local/upload  (constantly "local-ret")]
;      (is (= (.upload (->file "ftp://host:80/some/file/path.txt")) "ftp://host:80/some/file/path.txt"))
;      (is (= (.upload (->file "https://host:80/some/file/path.txt")) "https://host:80/some/file/path.txt"))
;      (is (= (.upload (->file "http://host:80/some/file/path.txt")) "http://host:80/some/file/path.txt"))
;      (is (= (.upload (->file "sftp://host:80/some/file/path.txt")) "sftp://host:80/some/file/path.txt"))
;      (is (= (.upload (->file "oci://host:80/some/file/path.txt")) "oci://host:80/some/file/path.txt"))
;      (is (= (.upload (->file "/some/random/path.txt")) "/some/random/path.txt"))
;      (is (thrown? Exception (.upload (->file "s3://unsupported/today")))))))
