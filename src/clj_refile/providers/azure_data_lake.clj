(ns clj-refile.providers.azure-data-lake
  (:require [clj-refile.protocols :refer [FileProtocol]]
            [clj-refile.providers.extras.azure-credentials :as ac]
            [taoensso.timbre :as log])
  (:import (com.azure.storage.file.datalake DataLakeFileClient DataLakePathClientBuilder)))

(defn- create-data-lake-file-client
  [conn-props filepath]
  (let [azf (ac/uri->azure-parsed filepath conn-props)]
    (log/debug "Creating Azure Data Lake File Client")
    (-> (DataLakePathClientBuilder.)
        (.endpoint (:endpoint azf))
        (ac/authenticate azf)
        (.fileSystemName (:file-container azf))
        (.pathName (:file-location azf))
        (.buildFileClient))))

(defrecord AzureDataLakeFile [scheme filepath conn-props]
  FileProtocol
  (checksum [_]
    (let [^DataLakeFileClient client (create-data-lake-file-client conn-props filepath)
          file-properties (.getProperties client)]
      (or (-> file-properties .getMetadata (get "checksum"))
          (.getContentMd5 file-properties))))
  (delete-file [_]
    (let [^DataLakeFileClient client (create-data-lake-file-client conn-props filepath)]
      (.delete client))
    true)
  (delete-if-exists [this]
    (when (.exists? this) (.delete-file this)))
  (download [_ output]
    (let [^DataLakeFileClient client (create-data-lake-file-client conn-props filepath)]
      (.make-parents output)
      (.delete-if-exists output)
      (.readToFile client (:filepath output)))
    (:filepath output))
  (exists? [_]
    (let [^DataLakeFileClient client (create-data-lake-file-client conn-props filepath)]
      (.exists client)))
  (list-files [this]
    (when (.exists? this)
      [filepath]))
  (upload [_ input]
    (let [^DataLakeFileClient client (create-data-lake-file-client conn-props filepath)
          meta-data {"checksum" (.checksum input)}]
      (.uploadFromFile client (:filepath input))
      (.setMetadata client meta-data))
    filepath))