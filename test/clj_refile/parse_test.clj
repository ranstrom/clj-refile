(ns clj-refile.parse-test
  (:require [clojure.test :refer [deftest is testing]]
            [clj-refile.parse :as p]
            [clj-refile.fixtures :as fx])
  (:import [java.net URI]))

(deftest test-generated
  (is (fx/qc-test `p/parse-url))
  (is (fx/qc-test `p/parse-uri))
  (is (fx/qc-test `p/parse-transient)))

(deftest test-replace-conn-props-in-path
  (testing "getting file cache path"
    (is (= (p/replace-conn-props-in-path "ftp://{{user}}:{{pswd}}@example:80/some/path" {:user "darth" :pswd "vader"})
           "ftp://darth:vader@example:80/some/path"))))

(deftest test-parse-url
  (testing "helper for parsing URL string"
    (is (= (p/parse-url "http://example.com:80/some/fancy/path/index.html?name=limited#CHECKING")
           {:protocol  "http"
            :authority "example.com:80"
            :host      "example.com"
            :port      80
            :path      "/some/fancy/path/index.html"
            :query     "name=limited"
            :filename  "/some/fancy/path/index.html?name=limited"
            :ref       "CHECKING"}))
    (is (thrown? Exception (p/parse-url "sftp://example.com:22/fancy/path")))
    (is (thrown? Exception (p/parse-url "/example/fancy/path")))))

(deftest test-parse-uri
  (testing "helper for parsing URL string"
    (is (= (p/parse-uri "http://example.com:80/some/fancy/path/index.html?name=limited#CHECKING")
           {:scheme    "http"
            :authority "example.com:80"
            :host      "example.com"
            :port      80
            :path      "/some/fancy/path/index.html"
            :query     "name=limited"
            :fragment  "CHECKING"}))
    (is (= (p/parse-uri "sftp://example.com:22/fancy/path")
           {:scheme    "sftp"
            :authority "example.com:22"
            :host      "example.com"
            :port      22
            :path      "/fancy/path"
            :query     nil
            :fragment  nil}))
    (is (= (p/parse-uri "/example/fancy/path")
           {:scheme    nil
            :authority nil
            :host      nil
            :port      -1
            :path      "/example/fancy/path"
            :query     nil
            :fragment  nil}))))

(deftest test-parse-transient
  (testing "parse transient"
    (is (= (p/parse-transient (URI. "https://bucket.host:443/some/file/path.csv"))
           {:bucket "bucket"
            :path "some/file/path.csv"
            :filename "path.csv"}))
    (is (= (p/parse-transient (URI. "http://bucket.host:443/some/file/path.csv"))
           {:bucket "bucket"
            :path "some/file/path.csv"
            :filename "path.csv"}))
    (is (= (p/parse-transient (URI. "oci://some.host:8080/bucket/some/file/path.csv"))
           {:bucket "bucket"
            :path "some/file/path.csv"
            :filename "path.csv"}))))