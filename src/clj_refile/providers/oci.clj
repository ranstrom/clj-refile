(ns clj-refile.providers.oci
  (:require [clj-refile.parse :as p]
            [clj-refile.protocols :refer [FileProtocol]]
            [clojure.string :as string])
  (:import (java.util HashMap)))

(defn- parse-uri-for-object-storage
  [uri-string]
  (let [uri (p/parse-uri uri-string)]
    {:bucket    (as->
                 (:path uri) p
                  (subs p (+ (string/index-of p "/b/") 3))
                  (string/split p #"\/")
                  (get p 0))
     :filename  (last (string/split (:path uri) #"\/"))
     :namespace (as->
                 (:path uri) p
                  (subs p (+ (string/index-of p "/n/") 3))
                  (string/split p #"\/")
                  (get p 0))
     :object    (as->
                 (:path uri) p
                  (subs p (+ (string/index-of p "/o/") 3)))}))

(defrecord OCIFile [scheme filepath conn-props]
  FileProtocol
  (download [_ output]
    (.make-parents output)
    (try
      (let [transient-map (parse-uri-for-object-storage filepath)
            input-map (HashMap. {"bucket"    (:bucket transient-map)
                                 "object"    (:object transient-map)
                                 "namespace" (:namespace transient-map)
                                 "localFile" (:filepath output)})]
        (.download input-map)
        (:filepath output))
      (catch Exception e
        (throw (Exception. (str "Error downloading Oracle ObjectStorage file: " e))))))
  (upload [_ input]
    (try
      (let [transient-map (parse-uri-for-object-storage filepath)
            input-map (HashMap. {"bucket"          (:bucket transient-map)
                                 "object"          (:object transient-map)
                                 "namespace"       (:namespace conn-props)
                                 "localFile"       (:filepath input)
                                 "contentType"     "text/csv" ; application/gzip
                                 "contentEncoding" "gzip"
                                 "contentLanguage" "en-US"})]
        (.upload input-map)
        filepath)
      (catch Exception e
        (throw (Exception. (str "Error uploading Oracle ObjectStorage file: " e)))))
    filepath))